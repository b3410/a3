package com.zuitt;
import java.util.ArrayList;
import java.util.HashMap;
public class Main {
    public static void main(String[] args) {
        ArrayList<String> topGames = new ArrayList<String>();
        HashMap<String, Integer> games = new HashMap<>()
        {{
            put("Pokemon Sword", 30);
            put("Pokemon Shield", 100);
            put("Mario Odyssey", 50);
            put("Luigi's Mansion 3", 15);
            put("Super Smash Bros. Ultimate", 20);
        }};
        games.forEach((key, value)->{
            System.out.printf("%s has %d stocks left.\n",key,value);

        });
        games.forEach((key, value)->{
            if (value <= 30) {
                topGames.add(key);
                System.out.printf("%s has been added to top games list!\n", key);
            }

        });
        System.out.println("Shop's top games:");
        System.out.println(topGames);




    }
}
